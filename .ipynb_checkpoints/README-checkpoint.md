<img src="https://pbs.twimg.com/media/E0m6c9FX0AIX0p1.png" style="height:50px" align="left"/> <br><br>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

***

The GEOROC Database (Geochemistry of Rocks of the Oceans and Continents) is a comprehensive collection of published analyses <br/> of igneous and metamorphic rocks and minerals. It contains major and trace element concentrations, radiogenic and nonradiogenic <br/> isotope ratios as well as analytical ages for whole rocks, glasses, minerals and inclusions. Metadata include geospatial and other <br/> sample information, analytical details and references.

The GEOROC Database was established at the Max Planck Institute for Chemistry in Mainz (Germany). In 2021, the database was <br/> moved to Göttingen University, where it continues to be curated as part of the DIGIS project of the Department of Geochemistry and <br/> Isotope Geology at the Geoscience Centre (GZG) and the University and State Library (SUB). Development for GEOROC 2.0 <br/> includes a new data model for greater interoperability, options to contribute data, and improved access to the database.

As part of the DIGIS project, a new API interface has been created for the GEOROC database, allowing easy access to its contents <br/>
with simple programming skills. Users can now query the database and retrieve data using the new API, making it more accessible <br/>
and useful for researchers and other interested parties. This notebook demonstrates the basic capabilities of GEOROC data access <br/> via the new DIGIS API. 

For feedback, questions and further information contact [Digis-Info](mailto:digis-info@uni-goettingen.de) directly.

***

# Jupyter-Notebooks

Developing Jupyter Notebooks to demonstrate capabilities of GEOROC 2.0. Specifically data access via the API followed by <br/> suggested workflows for data formatting, cleaning, re-sampling and plotting.

In the first instance, used to test API. 

***

Visit our first Notebook at Binder: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/georoc%2FJupyter/HEAD?labpath=DIGIS_GeoRoc.ipynb)

***

[Visit our official Website](https://georoc.mpch-mainz.gwdg.de/georoc/)

<a href="https://twitter.com/DIGISgeo?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-count="false">Follow @DIGISgeo</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>